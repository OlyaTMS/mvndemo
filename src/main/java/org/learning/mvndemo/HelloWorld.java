package org.learning.mvndemo;

import org.apache.commons.lang3.StringUtils;

public class HelloWorld {
    public static void main(String[] args) {
        System.out.println(StringUtils.upperCase("Hello World"));
    }
    public String do1(){
        return "one";
    }
    String do2(){
        return "two";
    }
    protected String do3(){
        return do4() + "three";
    }
    private String do4(){
        return "four";
    }


}
